Notes, exercises and problem solutions for the [Neural Networks and Deep Learning](http://neuralnetworksanddeeplearning.com/exercises_and_problems.html) in [Typed Racket](https://docs.racket-lang.org/ts-guide/)

When in the racket directory, open a Racket interpreter and run something like:

```lisp
(require "../mnist-reader.rkt")
(require "../network.rkt")
(define data-directory "/Users/xiaodili/dev/neural_networks_and_deep_learning/src/data")
(define-values (training-data test-data) (load-data data-directory))
(displayln "finished loading data")

(let* ([epochs 20]
       [mini-batch-size 10]
       [eta 3.0]
       [network (new network% [sizes '(784 30 10)])])
  (send network sgd training-data epochs mini-batch-size eta test-data))
```
