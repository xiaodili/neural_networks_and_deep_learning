import os
import struct
import numpy as np

# Adapted from https://gist.github.com/akesling/5358964

def format_label(j):
  e = np.zeros((10, 1))
  e[j] = 1.0
  return e


# Get it into book format (split the training_images into a validation set
def format_data(_training_images, _training_labels, _test_images, _test_labels):
  normaliseImageIntensity = np.vectorize(lambda x: x / 255)

  training_images = normaliseImageIntensity(map(lambda x: x.astype(np.float32), _training_images[:50000]))
  training_labels = map(lambda x: x.astype(np.float64), map(format_label, _training_labels[:50000]))

  validation_images = normaliseImageIntensity(map(lambda x: x.astype(np.float32), _training_images[50000:]))
  validation_labels = map(lambda x: x.astype(np.float64), _training_labels[50000:])

  test_images = normaliseImageIntensity(map(lambda x: x.astype(np.float32), _test_images))
  test_labels = map(lambda x: x.astype(np.float64), _test_labels)

  return (zip(training_images, training_labels), zip(validation_images, validation_labels), zip(test_images, test_labels))

def load_data(path):
  training_image_path = os.path.join(path, 'train-images-idx3-ubyte')
  training_label_path = os.path.join(path, 'train-labels-idx1-ubyte')
  test_image_path = os.path.join(path, 't10k-images-idx3-ubyte')
  test_label_path = os.path.join(path, 't10k-labels-idx1-ubyte')

  with open(training_label_path, 'rb') as f:
    magic, num = struct.unpack(">II", f.read(8))
    training_labels = list(np.fromfile(f, dtype=np.int8))
  with open(training_image_path, 'rb') as f:
    magic, num, rows, cols = struct.unpack(">IIII", f.read(16))
    training_images = list(np.fromfile(f, dtype=np.uint8).reshape(len(training_labels), rows * cols, 1))
  with open(test_label_path, 'rb') as f:
    magic, num = struct.unpack(">II", f.read(8))
    test_labels = list(np.fromfile(f, dtype=np.int8))
  with open(test_image_path, 'rb') as f:
    magic, num, rows, cols = struct.unpack(">IIII", f.read(16))
    test_images = list(np.fromfile(f, dtype=np.uint8).reshape(len(test_labels), rows * cols, 1))
  return format_data(training_images, training_labels, test_images, test_labels)

def show(image):
  from matplotlib import pyplot
  import matplotlib as mpl
  fig = pyplot.figure()
  ax = fig.add_subplot(1,1,1)
  imgplot = ax.imshow(image, cmap=mpl.cm.Greys)
  imgplot.set_interpolation('nearest')
  ax.xaxis.set_ticks_position('top')
  ax.yaxis.set_ticks_position('left')
  pyplot.show()
