import random
import numpy as np

#### Miscellaneous functions
def sigmoid(z):
  """The sigmoid function."""
  return 1.0/(1.0+np.exp(-z))

def sigmoid_prime(z):
  """Derivative of the sigmoid function."""
  return sigmoid(z)*(1-sigmoid(z))

def feedforward(biases, weights, a):
  for b, w in zip(biases, weights):
    a = sigmoid(np.dot(w, a)+b)
  return a

def cost_derivative(output_activations, y):
  return (output_activations-y)

def evaluate(test_data, weights, biases):
  test_results = [(np.argmax(feedforward(biases, weights, x)), y) for (x, y) in test_data]
  return sum(int(x == y) for (x, y) in test_results)

def backprop(biases, weights, num_layers, x, y):
  nabla_b = [np.zeros(b.shape) for b in biases]
  nabla_w = [np.zeros(w.shape) for w in weights]
  # feedforward
  activation = x
  activations = [x] # list to store all the activations, layer by layer
  zs = [] # list to store all the z vectors, layer by layer
  for b, w in zip(biases, weights):
    z = np.dot(w, activation)+b
    zs.append(z)
    activation = sigmoid(z)
    activations.append(activation)
  # backward pass
  delta = cost_derivative(activations[-1], y) * \
    sigmoid_prime(zs[-1])
  nabla_b[-1] = delta
  nabla_w[-1] = np.dot(delta, activations[-2].transpose())
  for l in xrange(2, num_layers):
    z = zs[-l]
    sp = sigmoid_prime(z)
    delta = np.dot(weights[-l+1].transpose(), delta) * sp
    nabla_b[-l] = delta
    nabla_w[-l] = np.dot(delta, activations[-l-1].transpose())
  return (nabla_b, nabla_w)

def update_mini_batch(weights, biases, num_layers, mini_batch, eta):
  nabla_b = [np.zeros(b.shape) for b in biases]
  nabla_w = [np.zeros(w.shape) for w in weights]
  for x, y in mini_batch:
    delta_nabla_b, delta_nabla_w = backprop(biases, weights, num_layers, x, y)
    nabla_b = [nb+dnb for nb, dnb in zip(nabla_b, delta_nabla_b)]
    nabla_w = [nw+dnw for nw, dnw in zip(nabla_w, delta_nabla_w)]
  updated_weights = [w-(eta/len(mini_batch))*nw for w, nw in zip(weights, nabla_w)]
  updated_biases = [b-(eta/len(mini_batch))*nb for b, nb in zip(biases, nabla_b)]
  return (updated_weights, updated_biases)

class Network(object):
  def __init__(self, sizes):
    self.num_layers = len(sizes)
    self.sizes = sizes
    self.biases = [np.random.randn(y, 1) for y in sizes[1:]]
    self.weights = [np.random.randn(y, x) for x, y in zip(sizes[:-1], sizes[1:])]

  def SGD(self, training_data, epochs, mini_batch_size, eta, test_data=None):
    if test_data:
      n_test = len(test_data)
    n = len(training_data)
    for j in xrange(epochs):
      random.shuffle(training_data)
      mini_batches = [training_data[k:k+mini_batch_size]
        for k in xrange(0, n, mini_batch_size)]
      for mini_batch in mini_batches:
        self.weights, self.biases = update_mini_batch(self.weights, self.biases, self.num_layers, mini_batch, eta)
      if test_data:
        print "Epoch {0}: {1} / {2}".format(j, evaluate(test_data, self.weights, self.biases), n_test)
      else:
        print "Epoch {0} complete".format(j)

  def getInfo(self):
    return {
      'sizes': self.sizes,
      'biases': self.biases,
      'weights': self.weights
      }
